<?php

/**
 * Controller for managing users.
 * Author: Marc Steele
 */

App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController {

	var $components = array('Auth');

	/**
	 * Allow non-authenticated users to register
 	 */

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('register', 'validateToken', 'registerEmail');
	}

	/**
	 * The registration screen
	 */

	public function register() {

		if ($this->request->is('post')) {

			// Create the new user
			// We'll need to create a token to allow registration

			$this->User->create();
			$tokenBase = "{$this->request->data['User']['username']}{$this->request->data['User']['email']}";
			$this->request->data['User']['token'] = md5($tokenBase);

			// Try to save the user

			if ($this->User->save($this->request->data)) {

				// Send the e-mail out

				$confirmURL = Router::url(
					array(
						'controller' => 'users',
						'action' => 'validateToken',
						'?' => array(
							'username' => $this->request->data['User']['username'],
							'token' => $this->request->data['User']['token']
						)
					),
					true
				);

				$registerEmail = new CakeEmail();
				$registerEmail->from(array('noreply@gps.dlineradio.co.uk' => 'Pi Hunter'));
				$registerEmail->to($this->request->data['User']['email']);
				$registerEmail->subject("Confirm Registration");
				$registerEmail->send("Hello,\n\nTo confirm your new account for Pi Hunter, please visit {$confirmURL}.\n\nRegards,\n\nThe Pi Hunter Team");

				// Punt the user to the complete screen

				return $this->redirect(array('action' => 'registerEmail'));

			} else {

				// Error out

				$this->Session->setFlash(__('We ran into a problem creating your account. Please check your responses and try again.'));

			}	

		}

	}

	/**
	 * Pop-up screen for telling the user to check their inbox.
	 */

	public function registerEmail() {
	}

	/**
	 * Attempts to validate the user.
	 */

	public function validateToken() {

		$username = $this->request->query['username'];
		$token = $this->request->query['token'];

		// See if we can find the user

		$results = $this->User->findByUsername($username);
		if ($results == null || count($results) == 0) {
			throw new NotFoundException(__('We couldn\'t find the user account you supplied'));
		}

		// Check the token

		if ($results['User']['token'] != $token) {
			throw new NotFoundException(__('We couldn\'t find the user associated with the token you supplied.'));
		}

		// Make the user valid and save it

		$results['User']['valid'] = true;
		if (!$this->User->save($results)) {
			throw new CakeException(__('We ran into a problem validating your account.'));
		}
		

		$this->Session->setFlash('You have successfully activated your account.');
		$this->redirect(array('action' => 'login'));

	}

	/**
	 * Allows users to login.
	 */

	public function login() {

		if ($this->request->is('post')) {

			// Hand crank the hashing / login

			if ($this->Auth->login(
				array(
					'User' => array(
						'username' => $this->data['User']['username'],
						'password' =>  AuthComponent::password($this->data['User']['password'])
					)
				)
			)) {
				return $this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash(__('Invalid username and/or password, please try again.'));
			}
	
		}

	}


	/**
	 * Allows users to log out.
	 */

	public function logout() {
		$this->redirect($this->Auth->logout());
	}

}

?>
