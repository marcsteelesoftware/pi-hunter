<?php

/**
 * Controller for managing tracker devices.
 */

class DevicesController extends AppController {

	public $uses = array('Device', 'Track');
	public $components = array('RequestHandler');

	// Shows a basic device management screen

	public function index() {

		// Get the devices for this user

		$currentUser = $this->User->find();
		$this->set('devices', $currentUser['Device']);

	}

	// Make sure we're allowed in

	public function isAuthorized($user) {
		return parent::isAuthorized($user);
	}

	public function beforeFilter() {

		parent::beforeFilter();
		$this->Auth->allow('track', 'map', 'history');

	}

	// Allow the user to add new devices

	public function add() {

		if ($this->request->is('post')) {

			// Create the new entry

			$this->Device->create();
			$this->request->data['Device']['guid'] = String::uuid();
			$this->request->data['Device']['key'] = md5(time());
			
			// Associate it with the user


			$currentUser = $this->User->find();
			$this->request->data['Device']['user_id'] = $currentUser['User']['id'];

			// Try to save it

			if (!$this->Device->save($this->request->data)) {
				$this->Session->setFlash(__('We ran into a problem adding the new device'));
				return;
			}
			
			// Tell the user what's up and redirect
			
			$this->Session->setFlash(__('Successfully added the new device to the system.'));
			$this->redirect(array('action' => 'index'));

		}

	}

	// Allow the user to edit the device

	public function edit($guid) {

		// Make sure we've got a valid device

		$currentDevice = $this->Device->findByGuid($guid);
		$currentUser = $this->Auth->user();

		if (!$currentDevice || $currentDevice['User']['username'] != $currentUser['User']['username']) {
			throw new NotFoundException(__('Could not find the device or you\'re not allowed access to the device'));
		}

		// Attempt to save

		if ($this->request->is('post') || $this->request->is('put')) {

			// Make sure the database IDs match

			if ($this->request->data['Device']['id'] != $currentDevice['Device']['id']) {

				$this->Session->setFlash(__('Detected a security issue. Bailing out...'));
				return $this->redirect(array('controller' => 'devices', 'action' => 'index'));

			}

			// Perform the save process

			if ($this->Device->save($this->request->data)) {
				
				$this->Session->setFlash(__('The changes to the device have been successfully changed.'));
				return $this->redirect(array('controller' => 'devices', 'action' => 'index'));
				
			} else {

				$this->Session->setFlash(__('Failed to save the changes to the device.'));

			}

		}

		// Load the data from the original if we don't have anything

		if (!$this->request->data) {
			$this->request->data = $currentDevice;
		}

	}

	// Allow the user to delete the device

	public function delete($guid) {

		// Make sure we've got a valid device

                $currentDevice = $this->Device->findByGuid($guid);
                $currentUser = $this->Auth->user();

                if (!$currentDevice || $currentDevice['User']['username'] != $currentUser['User']['username']) {
                        throw new NotFoundException(__('Could not find the device or you\'re not allowed access to the device'));
                }

		// Attempt to delete

		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->Device->delete($currentDevice['Device']['id'])) {

				$this->Session->setFlash(__('Successfully deleted the device.'));
				return $this->redirect(array('controller' => 'devices', 'action' => 'index'));

			} else {

				$this->Session->setFlash(__('Looks like we ran into a problem trying to delete the device.'));

			}

		}

		// Load the device details to allow the user to confirm

		if (!$this->request->data) {
			$this->request->data = $currentDevice;
		}

	}

	// Add tracks to devices

	public function track() {

		// Configure this to be plain text

		$this->layout = false;
		$this->RequestHandler->respondAs('text');

		// Pull in the values from the tracking request

		$unixTimestamp = $this->request->query['timestamp'];
		$guid = $this->request->query['guid'];
		$longitude = $this->request->query['longitude'];
		$latitude = $this->request->query['latitude'];
		$speed = $this->request->query['speed'];
		$signature = $this->request->query['sig'];

		// Check we've got a valid device

		$device = $this->Device->findByGuid($guid);
		if (!$device) {
			$this->set('success', false);
			$this->set('message', 'We couldn\'t find a device with that GUID.');
			return;
		}

		// Check the signiture

		$sigRaw = "guid=$guid&latitude=$latitude&longitude=$longitude&speed=$speed&timestamp=$unixTimestamp";
		$referenceSig = hash_hmac('sha256', $sigRaw, $device['Device']['key']);
		if ($signature != $referenceSig) {
			$this->set('success', false);
			$this->set('message', "The supplied signature doesn't match the expected signature.");
			return;	
		}

		// If we get this far, log the track

		$trackData = array(
			'Track' => array(
				'device_id' => $device['Device']['id'],
				'timestamp' => date('Y-m-d H:i:s', $unixTimestamp),
				'longitude' => $longitude,
				'latitude' => $latitude,
				'speed' => $speed
			)
		);

		if ($this->Track->save($trackData)) {
			$this->set('success', true);
			$this->set('message', 'Added the track to the database.');
		} else {
			$this->set('success', false);
			$this->set('message', 'Failed to save the track to the database.');
		}

	}

	// Displays a pretty map to the user

	public function map($guid) {

		// Hunt out the device

                $device = $this->Device->findByGuid($guid);
                if (!$device) {
                        $this->Session->setFlash(__('We couldn\'t find the device you\'re interested in.'));
                        return $this->redirect(array('controller' => 'devices', 'action' => 'index'));
                }

		// Check we're allowed to show it

		if (!$device['Device']['public']) {

                        $currentUser = $this->Auth->user();
                        if (!$currentUser || $device['User']['username'] != $currentUser['User']['username']) {
                                $this->Session->setFlash(__('You do not have permission to access this device.'));
                                return $this->redirect(array('controller' => 'devices', 'action' => 'index'));
                        }
                }

		// Show the screen

		$this->set('device', $device);
		

	}

	// Presents the history of the device in the KML format

	public function history($guid) {

		// Force the response to be plain text

		$this->layout = false;
		$this->RequestHandler->respondAs('text');

		// Hunt out the device

		$device = $this->Device->findByGuid($guid);
		if (!$device) {
			$this->set('success', false);
			$this->set('error', 'We couldn\'t find the device you\'re interested in.');
			return;
		}

		// Check we're allowed to show it

		if (!$device['Device']['public']) {

			$currentUser = $this->Auth->user();
			if (!$currentUser || $device['User']['username'] != $currentUser['User']['username']) {
				$this->set('success', false);
				$this->set('error', 'You do not have permission to access this device.');
				return;
			}
		}

		// Sort the tracks
		/*
		usort($device['Track'], function($a, $b) {

			$timeA = time($a['timestamp']);
			$timeB = time($b['timestamp']);

			if ($timeA == $timeB) {
				return 0;
			} elseif ($timeA > $timeB) {
				return -1;
			} else {
				return 1;
			}

		});
		*/
		// Let's give the device tracks over

		$this->RequestHandler->respondAs('application/vnd.google-earth.kml+xml');
		header('Access-Control-Allow-Origin: *');
		$this->set('success', true);
		$this->set('tracks', $device['Track']);	
		$this->set('guid', $guid);
		$this->set('name', $device['Device']['name']);

	}

}

?>
