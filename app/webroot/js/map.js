/**
 * Map Interface for Pi Hunter
 * Author: Marc Steele
 */

MQA.EventUtil.observe(window, 'load', function() {

	// Find out where we're getting our KML data from

	var kml_url = document.getElementById("kml_url").value;
	if (!kml_url) {
		alert('You didn\'t tell us which device you want to track!');
		return;
	}

	// Show a map

	var map_options = {
		elt: document.getElementById("track_map"),
		latLng: {lat: 52.943724, lng: -1.186019},
		zoom: 8,
		mtype: 'map'
	}

	var map = new MQA.TileMap(map_options);
	

	// Add the controls

	MQA.withModule('largezoom', function() {
		map.addControl(
			new MQA.LargeZoom(),
			new MQA.MapCornerPlacement(MQA.MapCorner.TOP_LEFT, new MQA.Size(5,5))
		);
	}); 

	// Load the KML layer

	MQA.withModule('dotcomwindowmanager','remotecollection','kmldeserializer', function() {

		var gps_track = new MQA.RemoteCollection(kml_url, new MQA.KMLDeserializer());
		gps_track.collectionName = 'GPS Track';

		MQA.EventManager.addListener(gps_track, 'loaded', function() {
			map.bestFit();
		});

		map.addShapeCollection(gps_track);

	});

});
