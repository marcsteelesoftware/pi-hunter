<?php
/**
 * Represents a GPS tracker device in the system.
 * Author: Marc Steele
 */

class Device extends AppModel {

	public $belongsTo = 'User';
	public $hasMany = 'Track';

	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'A device name is required'
			)
		),
		'guid' => array(
			'required' => array(
				'rule' => array('notEmpty', 'unique'),
				'message' => 'The device must have a unique GUID'
			)

		)
	);

}

?>
