<?php

/**
 * Represents a valid track for a GPS device.
 * Author: Marc Steele
 */

class Track extends AppModel {

	public $belongsTo = 'Device';

	public $validate = array(
		'timestamp' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'A track must have a timestamp'
			)
		),
		'longitude' => array(
                        'required' => array(
                                'rule' => array('notEmpty'),
                                'message' => 'A track must have a longitude'
                        )
                ),
		'latitude' => array(
                        'required' => array(
                                'rule' => array('notEmpty'),
                                'message' => 'A track must have a latitude'
                        )
                ),
		'speed' => array(
                        'required' => array(
                                'rule' => array('notEmpty'),
                                'message' => 'A track must have a speed'
                        )
                ),
	);

}

?>
