<?php

/**
 * Authenticated user model.
 * Provides a basis for authenticating users.
 * Author: Marc Steele
 */

App::uses('AuthComponent', 'Controller/Compontent');

class User extends AppModel {

	public $hasMany = 'Device';

	// Field validation

	public $validate = array(
		'username' => array(
			'required' => array(
				'rule' => array('notEmpty', 'isUnique'),
				'message' => 'A unique, non-empty username is required'
			)
		),
		'password' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'A password is required'
			)
		),
		'email' => array(
			'required' => array(
				'rule' => array('email'),
				'message' => 'A valid e-mail address is required'
			)
		)
	);

	// Hash the password before saving

	public function beforeSave($options = array()) {

		if (isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}

		return true;

	}	

}

?>
