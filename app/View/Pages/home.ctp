<h2>Welcome to Pi Hunter</h2>
<p>
	Pi Hunter is a PHP application designed to track GPS information from a Raspberry Pi. Using it is as simple as installing
	the software and following the onscreen prompts.
</p>
<p>
	If you're a developer type, you'll probably already know that this is based on CakePHP and the client is written in Java.
</p>
