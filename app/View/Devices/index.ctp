<h2>Manage Devices</h2>
<table>
	<thead>
		<th>Name</th>
		<th>Public</th>
		<th>GUID</th>
		<th>Key</th>
		<th>Actions</th>
	</thead>
	<tbody>
		<?php foreach ($devices as $currentDevice): ?>
			<tr>
				<td><?php echo $currentDevice['name']; ?></td>
				<td><?php echo $currentDevice['public'] ? 'Yes' : 'No'  ?></td>
				<td><?php echo $currentDevice['guid']; ?></td>
				<td><?php echo $currentDevice['key']; ?></td>
				<td>
					<?php echo $this->Html->link('View on Map', array('controller' => 'devices', 'action' => 'map', $currentDevice['guid'])); ?>
					<?php echo $this->Html->link('Edit', array('controller' => 'devices', 'action' => 'edit', $currentDevice['guid'])); ?>
					<?php echo $this->Html->link('Delete', array('controller' => 'devices', 'action' => 'delete', $currentDevice['guid'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<p>
        To add a new device, <?php echo $this->Html->link('click here', array('controller' => 'devices', 'action' => 'add')) ?>.
</p>

