<div class="devices form">
	<?php echo $this->Form->create('Device'); ?>
	<fieldset>
		<legend><?php echo __('Delete Device') ?></legend>
		<p>
			Are you sure you want to delete the device?
		</p>
	</fieldset>
	<?php echo $this->Form->end(__('Yes, Delete It!')); ?>
</div>
