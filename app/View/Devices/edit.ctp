<div class="devices form">
	<?php echo $this->Form->create('Device'); ?>
	<fieldset>
		<legend><?php echo __('Edit Device') ?></legend>
		<?php
			echo $this->Form->input('name');
			echo $this->Form->input('public', array('label' => 'Make public? Tick the box if you want the track to be publically available.'));
			echo $this->Form->hidden('id');
			echo $this->Form->hidden('guid');
			echo $this->Form->hidden('key');
			echo $this->Form->hidden('user_id');
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Save')); ?>
</div>
