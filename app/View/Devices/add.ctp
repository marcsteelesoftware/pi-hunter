<div class="devices form">
	<?php echo $this->Form->create('Device'); ?>
	<fieldset>
		<legend><?php echo __('Add a New Device') ?></legend>
		<?php
			echo $this->Form->input('name');
			echo $this->Form->input('public', array('label' => 'Make public? Tick the box if you want the track to be publically available.'));
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Add')); ?>
</div>
