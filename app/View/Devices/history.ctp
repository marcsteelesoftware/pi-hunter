<?php
if ($success) {
	echo '<?xml version="1.0" encoding="UTF-8"?>';
?>

<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
	<Document>
		<name><?php print $guid; ?></name>
		<Style id="multiTrack_n10">
			<IconStyle>
				<Icon>
					<href>http://earth.google.com/images/kml-icons/track-directional/track-0.png</href>
				</Icon>
			</IconStyle>
			<LineStyle>
				<color>99ffac59</color>
				<width>6</width>
			</LineStyle>
		</Style>
		<Style id="multiTrack_h10">
			<IconStyle>
				<scale>1.2</scale>
				<Icon>
					<href>http://earth.google.com/images/kml-icons/track-directional/track-0.png</href>
				</Icon>
			</IconStyle>
			<LineStyle>
				<color>99ffac59</color>
				<width>8</width>
			</LineStyle>
		</Style>
		<StyleMap id="multiTrack20">
			<Pair>
				<key>normal</key>
				<styleUrl>#multiTrack_n10</styleUrl>
			</Pair>
			<Pair>
				<key>highlight</key>
				<styleUrl>#multiTrack_h10</styleUrl>
			</Pair>
		</StyleMap>
		<Placemark>
			<name><?php print $name; ?></name>
			<description>Track for <?php print $name; ?></description>
			<styleUrl>#multiTrack20</styleUrl>
			<LineString>
				<coordinates>
					<?php
						foreach ($tracks as $currentTrack) {
							echo "{$currentTrack['longitude']},{$currentTrack['latitude']},0 ";
						}
					?>
				</coordinates>
			</LineString>
		</Placemark>
		<?php 
			if (count($tracks) > 0) { 

				$lastKnown = end($tracks);
		?>
			<Placemark>
				<name><?php echo $name; ?></name>
				<description>Last known position for <?php echo $name; ?> at <?php echo $currentTrack['timestamp']; ?>.</description>
				<Point>
					<coordinates><?php echo $currentTrack['longitude']; ?>,<?php echo $currentTrack['latitude']; ?>,0</coordinates>
				</Point>
			</Placemark>
		<?php } ?>
	</Document>
</kml>

<?php
} else {
?>

Error: <?php echo $error; ?>

<?php
}
?>
