<div class="users form">
	<?php
		echo $this->Session->flash('auth');
		echo $this->Form->create('User');
	?>
	<fieldset>
		<legend><?php echo __('Login to Pi Hunter') ?></legend>
		<?php
			echo $this->Form->input('username');
			echo $this->Form->input('password');
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Login')); ?>
</div>

<p>
	If you do not have an account, please <?php echo $this->HTML->link('click here', array('controller' => 'users', 'action' => 'register')); ?> to register.
</p>
