<h2>Registration Complete</h2>
<p>
	Thanks for registering your new account. Unfortunately, you will not be able to use it until you have followed the link
	in the e-mail we just sent you.
</p>
